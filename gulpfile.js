// GULP variable declarations
var gulp = require('gulp'),
  sass = require('gulp-ruby-sass'),
  prefix = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  rename = require('gulp-rename'),
  minifycss = require('gulp-minify-css'),
  uglify = require('gulp-uglify');

// Process SASS functionality
gulp.task('process-scss', function() {
  return sass('docroot/includes/style/style.scss', { style: 'expanded', compass: true, })
    .pipe(prefix(['last 2 versions']))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('docroot/source/'))
    .pipe(rename('style.css.min'))
    .pipe(minifycss())
    .pipe(gulp.dest('docroot/source/'))
    .on('error', function (err) {
      console.error('Error', err.message);
    })
});

// Process vendor SCRIPTS functionality
gulp.task('process-vendor-script', function() {
  return gulp.src('docroot/includes/js/vendor/**/*.js')
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('docroot/source/'))
    .pipe(rename('vendor.js.min'))
    .pipe(uglify())
    .pipe(gulp.dest('docroot/source/'))
    .on('error', function (err) {
      console.error('Error', err.message);
    })
});

// Process custom SCRIPTS functionality
gulp.task('process-custom-script', function() {
  return gulp.src('docroot/includes/js/custom/**/*.js')
    .pipe(concat('script.js'))
    .pipe(gulp.dest('docroot/source/'))
    .pipe(rename('script.js.min'))
    .pipe(uglify())
    .pipe(gulp.dest('docroot/source/'))
    .on('error', function (err) {
      console.error('Error', err.message);
    })
});

// Setup the gulp WATCH functionality
gulp.task('default', function() {
  gulp.start('process-scss');
  gulp.start('process-vendor-script');
  gulp.start('process-custom-script');
  gulp.watch('docroot/includes/style/**/*.scss', ['process-scss']);
  gulp.watch('docroot/includes/js/vendor/**/*.js', ['process-vendor-script']);
  gulp.watch('docroot/includes/js/custom/**/*.js', ['process-custom-script']);
});
